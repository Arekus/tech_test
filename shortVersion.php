<?php
// implementation
define('SERVER_NULL_CONF', ['CPU' => 0, 'RAM' => 0, 'HDD' => 0]);

function calculate(array $serverType, array $vms): int
{
    validateVmList($vms);

    $currentServer = SERVER_NULL_CONF;
    $counter = 0;
    $index = 0;
    while (isset($vms[$index])) {
        $vm = $vms[$index];
        if (isConfigurationFit($serverType, $vm)) {
            if (!isConfigurationFit($currentServer, $vm)) {
                $counter++;
                $currentServer = $serverType;
            }

            $currentServer = reduceParams($currentServer, $vm);
        }
        $index++;
    }
    return $counter;
}

function validateVmList(array $vms): void
{
    if (!count($vms)) {
        throw new Exception('Vm list is empty');
    }
}

function isConfigurationFit(array $serverType, array $vm): bool
{
    foreach (array_keys(SERVER_NULL_CONF) as $field) {
        if ($serverType[$field] < $vm[$field]) {
            return false;
        }
    }
    return true;
}

function reduceParams(array $serverType, array $vm): array
{
    foreach (array_keys(SERVER_NULL_CONF) as $field) {
        $serverType[$field] -= $vm[$field];
    }
    return $serverType;
}

// data sets
$server = json_decode('{"CPU":2,"RAM":32,"HDD":100}', true);
$vmsSets = [
    [
        'set' => json_decode('[{"CPU":1,"RAM":16,"HDD":10},{"CPU":1,"RAM":16,"HDD":10},{"CPU":2,"RAM":32,"HDD":100}]', true),
        'expected' => 2
    ],
    [
        'set' => json_decode('[{"CPU":1,"RAM":16,"HDD":10},{"CPU":1,"RAM":16,"HDD":10},{"CPU":3,"RAM":32,"HDD":100}]', true),
        'expected' => 1
    ],
    [
        'set' => json_decode('[{"CPU":1,"RAM":16,"HDD":10},{"CPU":1,"RAM":16,"HDD":10},{"CPU":3,"RAM":32,"HDD":100},{"CPU":1,"RAM":16,"HDD":10},{"CPU":1,"RAM":16,"HDD":10},{"CPU":3,"RAM":32,"HDD":100},{"CPU":1,"RAM":16,"HDD":10},{"CPU":1,"RAM":16,"HDD":10},{"CPU":3,"RAM":32,"HDD":100},{"CPU":1,"RAM":16,"HDD":10},{"CPU":1,"RAM":16,"HDD":10},{"CPU":3,"RAM":32,"HDD":100},{"CPU":1,"RAM":16,"HDD":10},{"CPU":1,"RAM":16,"HDD":10},{"CPU":3,"RAM":32,"HDD":100},{"CPU":1,"RAM":16,"HDD":10},{"CPU":1,"RAM":16,"HDD":10},{"CPU":3,"RAM":32,"HDD":100}]', true),
        'expected' => 6
    ],
];

// testing
echo PHP_EOL . 'Result for empty vm list: ';
try {
    $result = calculate($server, []);
} catch (Exception $e) {
    echo 'Exception: ' . $e->getMessage();
}

foreach ($vmsSets as $vmSet) {
    echo PHP_EOL . PHP_EOL . 'Server: ' . json_encode($server);
    echo PHP_EOL . 'Vms: ' . json_encode($vmSet['set']);
    $result = calculate($server, $vmSet['set']);

    echo PHP_EOL . 'Result: ' . $result . ($vmSet['expected'] === $result ? ' is correct' : ' is wrong');
}
