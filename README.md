# README #

### What is this repository for? ###

This is a server calculator. It calculates the number of servers (which have the same configuration) needed to host a specified
amount of virtual machines. 

### Task description ###

The algorithm for the virtual machine distribution should implement a 'first fit' strategy. 
This means there is no resource optimization or 'look back'.
Virtual machines are always allocated on the current or the next server (in case of limited resources).

If a virtual machine is too 'big' for a server, it should be skipped.
If the collection of virtual machines is empty, an exception should be thrown.

### How do I get set up and run? ###

Requirements: PHP 7.1+, Composer

Set up: `composer install`

Run: `php index.php`

Output (for another set of data `index.php` should be edited): 
```
Server: {"CPU":2,"RAM":32,"HDD":100}
Vms: [{"CPU":1,"RAM":16,"HDD":10},{"CPU":1,"RAM":16,"HDD":10},{"CPU":2,"RAM":32,"HDD":100}]
Result 2
```

Test run: `./vendor/bin/phpunit --bootstrap vendor/autoload.php test`

Metrics generation: `./vendor/bin/phpmetrics --report-html=/path/to/your/report/in/html ./src`

### Is there anything shorter and simpler? ###

Yes. `php shortVersion.php` 
