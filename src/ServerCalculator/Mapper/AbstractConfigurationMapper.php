<?php
declare(strict_types=1);

namespace ServerCalculator\Mapper;

use ServerCalculator\Entity\ConfigurationInterface;

abstract class AbstractConfigurationMapper implements ConfigurationMapperInterface
{
    private const CPU_FIELD = 'CPU';
    private const RAM_FIELD = 'RAM';
    private const HDD_FIELD = 'HDD';

    /**
     * @param array $configurationRaw
     * @return ConfigurationInterface
     */
    public function getConfigurationFromArray(array $configurationRaw): ConfigurationInterface
    {
        /** @var ConfigurationInterface $className */
        $className = $this->getClassName();
        $configuration = new $className(
            $configurationRaw[self::CPU_FIELD],
            $configurationRaw[self::RAM_FIELD],
            $configurationRaw[self::HDD_FIELD]
        );
        return $configuration;
    }

    /**
     * @param array $configurations
     * @return ConfigurationInterface[]
     */
    public function getManyConfigurationsFromArray(array $configurations): array
    {
        return array_map([$this, 'getConfigurationFromArray'], $configurations);
    }

    abstract public function getClassName(): string;
}