<?php
declare(strict_types=1);

namespace ServerCalculator\Mapper;

use ServerCalculator\Entity\ServerConfiguration;

class ServerConfigurationMapper extends AbstractConfigurationMapper
{
    /** @inheritdoc */
    public function getClassName(): string
    {
        return ServerConfiguration::class;
    }
}