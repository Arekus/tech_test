<?php
declare(strict_types=1);

namespace ServerCalculator\Mapper;

use ServerCalculator\Entity\VirtualMachineConfiguration;

class VirtualMachineConfigurationMapper extends AbstractConfigurationMapper
{
    /** @inheritdoc */
    public function getClassName(): string
    {
        return VirtualMachineConfiguration::class;
    }
}