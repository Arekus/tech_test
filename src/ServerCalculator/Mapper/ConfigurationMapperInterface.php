<?php
declare(strict_types=1);

namespace ServerCalculator\Mapper;

interface ConfigurationMapperInterface
{
    /**
     * @return string
     */
    public function getClassName(): string;
}