<?php
declare(strict_types=1);

namespace ServerCalculator\Helper;

use ServerCalculator\Entity\ServerConfiguration;
use ServerCalculator\Entity\VirtualMachineConfiguration;

class ServerConfigurationCalculator
{
    /**
     * @param ServerConfiguration $server
     * @param VirtualMachineConfiguration $vm
     * @return bool
     */
    public function isConfigurationFit(ServerConfiguration $server, VirtualMachineConfiguration $vm): bool
    {
        if (
            $server->getCpu() < $vm->getCpu()
            || $server->getRam() < $vm->getRam()
            || $server->getHdd() < $vm->getHdd()
        ) {
            return false;
        }
        return true;
    }

    /**
     * @param ServerConfiguration $server
     * @param VirtualMachineConfiguration $vm
     */
    function reduceServerParams(ServerConfiguration $server, VirtualMachineConfiguration $vm): void
    {
        $server->setCpu($server->getCpu() - $vm->getCpu());
        $server->setRam($server->getRam() - $vm->getRam());
        $server->setHdd($server->getHdd() - $vm->getHdd());
    }
}