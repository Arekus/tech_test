<?php
declare(strict_types=1);

namespace ServerCalculator\Entity;

interface ConfigurationInterface
{
    /**
     * @param int $cpu
     * @param int $ram
     * @param int $hdd
     */
    public function __construct(int $cpu, int $ram, int $hdd);

    /**
     * @return int
     */
    public function getCpu(): int;

    /**
     * @return int
     */
    public function getRam(): int;

    /**
     * @return int
     */
    public function getHdd(): int;
}