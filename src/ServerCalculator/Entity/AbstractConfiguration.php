<?php
declare(strict_types=1);

namespace ServerCalculator\Entity;

abstract class AbstractConfiguration implements ConfigurationInterface
{
    /** @var int */
    protected $cpu;
    /** @var int */
    protected $ram;
    /** @var int */
    protected $hdd;

    /** @inheritdoc */
    public function __construct(int $cpu, int $ram, int $hdd)
    {
        $this->cpu = $cpu;
        $this->ram = $ram;
        $this->hdd = $hdd;
    }

    /** @inheritdoc */
    public function getCpu(): int
    {
        return $this->cpu;
    }

    /** @inheritdoc */
    public function getRam(): int
    {
        return $this->ram;
    }

    /** @inheritdoc */
    public function getHdd(): int
    {
        return $this->hdd;
    }
}