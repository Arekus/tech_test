<?php
declare(strict_types = 1);

namespace ServerCalculator\Entity;

class ServerConfiguration extends AbstractConfiguration
{
    /**
     * @param int $cpu
     */
    public function setCpu(int $cpu): void
    {
        $this->cpu = $cpu;
    }

    /**
     * @param int $ram
     */
    public function setRam(int $ram): void
    {
        $this->ram = $ram;
    }

    /**
     * @param int $hdd
     */
    public function setHdd(int $hdd): void
    {
        $this->hdd = $hdd;
    }
}