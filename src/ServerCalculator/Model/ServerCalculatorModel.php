<?php
declare(strict_types=1);

namespace ServerCalculator\Model;

use ServerCalculator\Entity\ServerConfiguration;
use ServerCalculator\Helper\ServerConfigurationCalculator;

class ServerCalculatorModel
{
    /** @var ServerConfigurationCalculator */
    private $calculator;

    /**
     * @param ServerConfigurationCalculator $calculator
     */
    public function __construct(ServerConfigurationCalculator $calculator)
    {
        $this->calculator = $calculator;
    }

    /**
     * @param ServerConfiguration $serverType
     * @param array $virtualMachines
     * @return int
     */
    public function calculate(ServerConfiguration $serverType, array $virtualMachines): int
    {
        $currentServer = null;
        $counter = 0;
        foreach ($virtualMachines as $virtualMachine) {
            if (!$this->calculator->isConfigurationFit($serverType, $virtualMachine)) {
                continue;
            }
            if (!isset($currentServer) || !$this->calculator->isConfigurationFit($currentServer, $virtualMachine)) {
                $counter++;
                $currentServer = clone $serverType;
            }

            $this->calculator->reduceServerParams($currentServer, $virtualMachine);
        }
        return $counter;
    }
}