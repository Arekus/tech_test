<?php
declare(strict_types=1);

namespace ServerCalculator\Validator;

interface VirtualMachinesValidatorInterface extends ValidatorInterface
{
    /**
     * @param array $virtualMachines
     */
    public function setVirtualMachines(array $virtualMachines): void;
}