<?php
declare(strict_types=1);

namespace ServerCalculator\Validator;

use ServerCalculator\Entity\ServerConfiguration;

class IsVMListPopulatedValidator implements VirtualMachinesValidatorInterface
{
    /** @var ServerConfiguration[] */
    private $virtualMachines;

    /**
     * @inheritdoc
     */
    public function setVirtualMachines(array $virtualMachines): void
    {
        $this->virtualMachines = $virtualMachines;
    }

    /**
     * @inheritdoc
     */
    public function validate(): bool
    {
        return count($this->virtualMachines) > 0;
    }
}