<?php
declare(strict_types=1);

namespace ServerCalculator\Validator;

interface ValidatorInterface
{
    /**
     * @return bool
     */
    public function validate(): bool;
}