<?php
declare(strict_types=1);

namespace ServerCalculator;

use ServerCalculator\Entity\ServerConfiguration;
use ServerCalculator\Exception\ServerCalculatorException;
use ServerCalculator\Mapper\ServerConfigurationMapper;
use ServerCalculator\Mapper\VirtualMachineConfigurationMapper;
use ServerCalculator\Model\ServerCalculatorModel;
use ServerCalculator\Validator\VirtualMachinesValidatorInterface;

class ServerCalculatorService
{
    /** @var ServerCalculatorModel */
    private $calculatorModel;
    /** @var VirtualMachinesValidatorInterface */
    private $validator;
    /** @var ServerConfigurationMapper */
    private $serverConfigurationMapper;
    /** @var VirtualMachineConfigurationMapper */
    private $vmConfigurationMapper;

    /**
     * @param ServerCalculatorModel $calculatorModel
     * @param VirtualMachinesValidatorInterface $validator
     * @param ServerConfigurationMapper $serverConfigurationMapper
     * @param VirtualMachineConfigurationMapper $vmConfigurationMapper
     */
    public function __construct(
        ServerCalculatorModel $calculatorModel,
        VirtualMachinesValidatorInterface $validator,
        ServerConfigurationMapper $serverConfigurationMapper,
        VirtualMachineConfigurationMapper $vmConfigurationMapper
    )
    {
        $this->calculatorModel = $calculatorModel;
        $this->validator = $validator;
        $this->serverConfigurationMapper = $serverConfigurationMapper;
        $this->vmConfigurationMapper = $vmConfigurationMapper;
    }

    /**
     * @param array $serverTypeRaw
     * @param array $virtualMachinesRaw
     * @return int
     * @throws ServerCalculatorException
     */
    public function calculate(array $serverTypeRaw, array $virtualMachinesRaw): int
    {
        /** @var ServerConfiguration $server */
        $server = $this->serverConfigurationMapper->getConfigurationFromArray($serverTypeRaw);
        $virtualMachines = $this->vmConfigurationMapper->getManyConfigurationsFromArray($virtualMachinesRaw);

        $this->validator->setVirtualMachines($virtualMachines);
        if (!$this->validator->validate()) {
            throw new ServerCalculatorException('Virtual machines list is empty');
        }

        return $this->calculatorModel->calculate($server, $virtualMachines);
    }
}
