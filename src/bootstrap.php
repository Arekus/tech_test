<?php
declare(strict_types=1);

use ServerCalculator\Helper\ServerConfigurationCalculator;
use ServerCalculator\Mapper\ServerConfigurationMapper;
use ServerCalculator\Mapper\VirtualMachineConfigurationMapper;
use ServerCalculator\Model\ServerCalculatorModel;
use ServerCalculator\ServerCalculatorService;
use ServerCalculator\Validator\IsVMListPopulatedValidator;

require __DIR__ . '/../vendor/autoload.php';

function getServerCalculatorService(): ServerCalculatorService
{
    $serverConfigurationMapper = new ServerConfigurationMapper();
    $vmConfigurationMapper = new VirtualMachineConfigurationMapper();
    $calculator = new ServerConfigurationCalculator();
    $calculatorModel = new ServerCalculatorModel($calculator);
    $calculatorValidator = new IsVMListPopulatedValidator();
    return new ServerCalculatorService(
        $calculatorModel,
        $calculatorValidator,
        $serverConfigurationMapper,
        $vmConfigurationMapper
    );
}
