<?php
declare(strict_types=1);

namespace Test\ServerCalculator;

class ServerCalculatorValidatorTest extends AbstractServerCalculator
{
    /**
     * @dataProvider vmListProvider
     * @param array $vmList
     * @param bool $expected
     */
    public function testException(array $vmList, bool $expected)
    {
        $this->calculatorValidator->setVirtualMachines($vmList);
        $this->assertEquals($expected, $this->calculatorValidator->validate());
    }

    public function vmListProvider()
    {
        return [
          [[], false],
          [['sample list'], true],
        ];
    }
}