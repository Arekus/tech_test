<?php
declare(strict_types=1);

namespace Test\ServerCalculator;

use PHPUnit\Framework\TestCase;
use ServerCalculator\Helper\ServerConfigurationCalculator;
use ServerCalculator\Mapper\ServerConfigurationMapper;
use ServerCalculator\Mapper\VirtualMachineConfigurationMapper;
use ServerCalculator\Model\ServerCalculatorModel;
use ServerCalculator\ServerCalculatorService;
use ServerCalculator\Validator\IsVMListPopulatedValidator;

class AbstractServerCalculator extends TestCase
{
    /** @var ServerConfigurationMapper */
    protected $serverConfigurationMapper;
    /** @var VirtualMachineConfigurationMapper */
    protected $vmConfigurationMapper;
    /** @var ServerCalculatorModel */
    protected $calculatorModel;
    /** @var IsVMListPopulatedValidator */
    protected $calculatorValidator;
    /** @var ServerCalculatorService */
    protected $calculatorService;
    /** @var ServerConfigurationCalculator */
    protected $calculator;

    public function setUp()
    {
        parent::setUp();

        $this->serverConfigurationMapper = new ServerConfigurationMapper();
        $this->vmConfigurationMapper = new VirtualMachineConfigurationMapper();
        $this->calculator = new ServerConfigurationCalculator();
        $this->calculatorModel = new ServerCalculatorModel($this->calculator);
        $this->calculatorValidator = new IsVMListPopulatedValidator();
        $this->calculatorService = new ServerCalculatorService(
            $this->calculatorModel,
            $this->calculatorValidator,
            $this->serverConfigurationMapper,
            $this->vmConfigurationMapper
        );
    }
}