<?php
declare(strict_types=1);

namespace Test\ServerCalculator;

class ServerCalculatorServiceTest extends AbstractServerCalculator
{
    /**
     * @expectedException \ServerCalculator\Exception\ServerCalculatorException
     * @expectedExceptionMessage Virtual machines list is empty
     */
    public function testException()
    {
        $this->calculatorService->calculate(['CPU' => 2, 'RAM' => 32, 'HDD' => 100], []);
    }

    /**
     * @throws \ServerCalculator\Exception\ServerCalculatorException
     */
    public function testNormalStart()
    {
        $result = $this->calculatorService->calculate(
            ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
            [
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100]
            ]
        );
        $this->assertIsInt($result);
    }
}