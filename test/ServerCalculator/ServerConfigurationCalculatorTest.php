<?php
declare(strict_types=1);

namespace Test\ServerCalculator;

use ServerCalculator\Entity\ServerConfiguration;
use ServerCalculator\Entity\VirtualMachineConfiguration;

class ServerConfigurationCalculatorTest extends AbstractServerCalculator
{
    /**
     * @dataProvider comparisonsProvider
     * @param array $serverRaw
     * @param array $vmRaw
     * @param bool $expected
     */
    public function testComparisons(array $serverRaw, array $vmRaw, bool $expected)
    {
        /** @var ServerConfiguration $server */
        $server = $this->serverConfigurationMapper->getConfigurationFromArray($serverRaw);
        /** @var VirtualMachineConfiguration $vm */
        $vm = $this->vmConfigurationMapper->getConfigurationFromArray($vmRaw);
        $this->assertEquals($expected, $this->calculator->isConfigurationFit($server, $vm));
    }

    /**
     * @dataProvider reductionsProvider
     * @param array $serverRaw
     * @param array $vmRaw
     * @param array $expectedRaw
     */
    public function testReduction(array $serverRaw, array $vmRaw, array $expectedRaw)
    {
        /** @var ServerConfiguration $server */
        $server = $this->serverConfigurationMapper->getConfigurationFromArray($serverRaw);
        /** @var VirtualMachineConfiguration $vm */
        $vm = $this->vmConfigurationMapper->getConfigurationFromArray($vmRaw);
        /** @var ServerConfiguration $expected */
        $expected = $this->serverConfigurationMapper->getConfigurationFromArray($expectedRaw);
        $this->calculator->reduceServerParams($server, $vm);
        $this->assertEquals($expected, $server);
    }

    public function comparisonsProvider()
    {
        return [
            [
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                ['CPU' => 1, 'RAM' => 10, 'HDD' => 100],
                true
            ],
            [
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                ['CPU' => 3, 'RAM' => 10, 'HDD' => 100],
                false
            ],
            [
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                ['CPU' => 1, 'RAM' => 50, 'HDD' => 100],
                false
            ],
            [
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                ['CPU' => 1, 'RAM' => 30, 'HDD' => 1000],
                false
            ],
            [
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                true
            ],
        ];
    }

    public function reductionsProvider()
    {
        return [
            [
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                ['CPU' => 1, 'RAM' => 10, 'HDD' => 100],
                ['CPU' => 1, 'RAM' => 22, 'HDD' => 0]
            ],
            [
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                ['CPU' => 0, 'RAM' => 0, 'HDD' => 0]
            ],
            [
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                ['CPU' => 2, 'RAM' => 11, 'HDD' => 22],
                ['CPU' => 0, 'RAM' => 21, 'HDD' => 78]
            ],
        ];
    }
}