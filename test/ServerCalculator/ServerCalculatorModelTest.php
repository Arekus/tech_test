<?php
declare(strict_types=1);

namespace Test\ServerCalculator;

use ServerCalculator\Entity\ServerConfiguration;

class ServerCalculatorModelTest extends AbstractServerCalculator
{
    /**
     * @dataProvider configurationsProvider
     * @param array $serverTypeRaw
     * @param array $virtualMachinesRaw
     * @param int $expected
     */
    public function testCalculation(array $serverTypeRaw, array $virtualMachinesRaw, int $expected)
    {
        /** @var ServerConfiguration $server */
        $server = $this->serverConfigurationMapper->getConfigurationFromArray($serverTypeRaw);
        $virtualMachines = $this->vmConfigurationMapper->getManyConfigurationsFromArray($virtualMachinesRaw);
        $this->assertEquals($expected, $this->calculatorModel->calculate($server, $virtualMachines));
    }

    public function configurationsProvider()
    {
        return [
            [
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                [
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                ],
                2
            ],
            [
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                [
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 3, 'RAM' => 32, 'HDD' => 100],
                ],
                1
            ],
            [
                ['CPU' => 2, 'RAM' => 32, 'HDD' => 100],
                [
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 3, 'RAM' => 32, 'HDD' => 100],
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 3, 'RAM' => 32, 'HDD' => 100],
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 3, 'RAM' => 32, 'HDD' => 100],
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 3, 'RAM' => 32, 'HDD' => 100],
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 1, 'RAM' => 16, 'HDD' => 10],
                    ['CPU' => 3, 'RAM' => 32, 'HDD' => 100],
                ],
                5
            ],
        ];
    }
}