<?php
declare(strict_types=1);

require __DIR__ . '/src/bootstrap.php';

$serverJson = '{"CPU":2,"RAM":32,"HDD":100}';
$vsmJson = '[{"CPU":1,"RAM":16,"HDD":10},{"CPU":1,"RAM":16,"HDD":10},{"CPU":2,"RAM":32,"HDD":100}]';

$serverRawData = json_decode($serverJson, true);
$vmsRawData = json_decode($vsmJson, true);

echo PHP_EOL . PHP_EOL . 'Server: ' . $serverJson;
echo PHP_EOL . 'Vms: ' . $vsmJson;
try {
    echo PHP_EOL . 'Result ' . getServerCalculatorService()->calculate($serverRawData, $vmsRawData);
} catch (Exception $e) {
    echo PHP_EOL . 'Exception: ' . $e->getMessage();
}



